package com.testAccenture.Accenturetest.Controller;

import com.testAccenture.Accenturetest.Entity.Dto.UsersInsertBodyDto;
import com.testAccenture.Accenturetest.Service.UsersServiceImpl;
import com.testAccenture.Accenturetest.Util.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {

    @Autowired
    private UsersServiceImpl usersServiceImpl;

    @PostMapping({"","/"})
    public Response createUsers(@RequestBody UsersInsertBodyDto usersInsertBodyDto){
        Response response = usersServiceImpl.saveDataUsers(usersInsertBodyDto);
        return response;
    }

    @PutMapping({"/{id}/","/{id}"})
    public Response updateDataUsers(@RequestBody UsersInsertBodyDto usersInsertBodyDto, @PathVariable String id){
        Response response = usersServiceImpl.updateDateUsers(usersInsertBodyDto,id);
        return response;
    }

    @GetMapping({"/getDetail/{id}/","/getDetail/{id}"})
    public Response getDetailId(@PathVariable String id){
        Response response = usersServiceImpl.getDateUsers(id);
        return response;
    }

    @PutMapping({"/deleted/{id}/","/deleted/{id}"})
    public Response deleteId(@PathVariable String id){
        Response response = usersServiceImpl.deleteDateUsers(id);
        return response;
    }

    @GetMapping({"/getDetail/","/getDetail"})
    public Response getDetailAll(){

        Response response = usersServiceImpl.getDateUsersAll();
        return response;
    }

    @GetMapping({"/getDetailDel/","/getDetailDel"})
    public Response getDetailAllDeleted(){
        Response response = usersServiceImpl.getDateUsersAllDel();
        return response;
    }
}
