package com.testAccenture.Accenturetest.Entity;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;

@Entity
@Data
@Table
public class Users {

    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SOCIALSECURITYNUMBER",nullable=false)
    private String socialSecurityNumber;

    @Column(name = "NAME",nullable=false)
    private String name;

    @Column(name = "DATEOFBIRTH",nullable=false)
    private Date dateOfBirth;

    @CreationTimestamp
    @Column(name = "DATECREATED", nullable=false,insertable = true, updatable = false)
    protected Date dateCreated;

    @UpdateTimestamp
    @Column(name = "DATEUPDATED",insertable = true, updatable = true)
    private Date dateUpdated;

    @ColumnDefault("CURRENT_TIMESTAMP")
    @Column(name = "UPDATEDBY",nullable=false)
    private String updatedBy;

    @Column(name = "CREATEDBY",nullable=false)
    private String createdBy;

    @Column(name = "IS_DELETED",nullable=false)
    private Boolean isDeleted;

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
