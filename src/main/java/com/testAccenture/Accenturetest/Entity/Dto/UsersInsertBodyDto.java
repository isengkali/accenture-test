package com.testAccenture.Accenturetest.Entity.Dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsersInsertBodyDto {
    private String socialSecurityNumber;
    private String name;
    private String dateOfBirth;
    private String createdBy;
    private String updatedBy;

}
