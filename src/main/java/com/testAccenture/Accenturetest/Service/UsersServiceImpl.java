package com.testAccenture.Accenturetest.Service;

import com.testAccenture.Accenturetest.Entity.Dto.UsersInsertBodyDto;
import com.testAccenture.Accenturetest.Entity.Users;
import com.testAccenture.Accenturetest.Repository.UsersRepository;
import com.testAccenture.Accenturetest.Util.CustomException;
import com.testAccenture.Accenturetest.Util.Response;
import com.testAccenture.Accenturetest.validator.UsersValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class UsersServiceImpl {

    private final UsersRepository usersRepository;
    private final UsersValidator usersValidator;

    public UsersServiceImpl(UsersRepository usersRepository, UsersValidator usersValidator) {
        this.usersRepository = usersRepository;
        this.usersValidator = usersValidator;
    }

    public Response saveDataUsers(UsersInsertBodyDto usersInsertBodyDto) {
    Response response = new Response();
    try{
        response = usersValidator.addUsersValidator(usersInsertBodyDto);
        if(response.getResult() ==null){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            simpleDateFormat.setLenient(false);
            Users insertUser = new Users();
            insertUser.setSocialSecurityNumber(String.format("%05d", Integer.valueOf(usersInsertBodyDto.getSocialSecurityNumber())));
            insertUser.setName(usersInsertBodyDto.getName());
            insertUser.setDateOfBirth(simpleDateFormat.parse(usersInsertBodyDto.getDateOfBirth().trim()));
            insertUser.setDateCreated(new Date());
            insertUser.setDateUpdated(new Date());
            insertUser.setCreatedBy(usersInsertBodyDto.getCreatedBy() != null ? usersInsertBodyDto.getCreatedBy() : "SPRING_BOOT_TEST");
            insertUser.setUpdatedBy(usersInsertBodyDto.getUpdatedBy() != null ? usersInsertBodyDto.getUpdatedBy() : "SPRING_BOOT_TEST");
            insertUser.setIsDeleted(false);
            usersRepository.save(insertUser);
            response.setCode(201L);
            response.setMessage("Success Insert Users");
            response.setStatus("Success");
            response.setResult(insertUser);
        }
    }catch (Exception e){
        e.printStackTrace();
        response.setStatus("Failed");
        response.setMessage(e.getMessage());
        response.setResult("");
    }
        return response;
    }

    public Response updateDateUsers(UsersInsertBodyDto usersInsertBodyDto, String id)
    {
        Response response = new Response();
        try{
            response = usersValidator.updateUsersValidator(usersInsertBodyDto, id);
            if(response.getResult() ==null){
                Users accountStatementCredit = usersRepository.findBySocialSecurityNumber(String.format("%05d", Integer.valueOf(id)));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                simpleDateFormat.setLenient(false);
                Users insertUser = new Users();
                insertUser.setSocialSecurityNumber(accountStatementCredit.getSocialSecurityNumber());
                insertUser.setName(usersInsertBodyDto.getName() != null ? usersInsertBodyDto.getName() : accountStatementCredit.getName());
                insertUser.setDateOfBirth(usersInsertBodyDto.getDateOfBirth() != null ? simpleDateFormat.parse(usersInsertBodyDto.getDateOfBirth().trim()) : accountStatementCredit.getDateOfBirth());
                insertUser.setDateCreated(accountStatementCredit.getDateCreated());
                insertUser.setDateUpdated(new Date());
                insertUser.setCreatedBy(usersInsertBodyDto.getCreatedBy() != null ? usersInsertBodyDto.getCreatedBy() : accountStatementCredit.getCreatedBy());
                insertUser.setUpdatedBy(usersInsertBodyDto.getUpdatedBy() != null ? usersInsertBodyDto.getUpdatedBy() : accountStatementCredit.getUpdatedBy());
                insertUser.setIsDeleted(false);
                usersRepository.save(insertUser);
                response.setCode(201L);
                response.setMessage("Success Insert Users");
                response.setStatus("Success");
                response.setResult(insertUser);
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus("Failed");
            response.setMessage(e.getMessage());
            response.setResult("");
        }
        return response;
    }
    public Response getDateUsers(String id)
    {
        Response response = new Response();
        try{
            response = usersValidator.getUsersValidator(id);
            if(response.getResult() ==null){
                List<Users> usersList = usersRepository.findBySocialSecurityNumberAndIsDeletedFalse(String.format("%05d", Integer.valueOf(id)));
                response.setResult(usersList);
                response.setCode(200L);
                response.setMessage("Success Get data Users");
                response.setStatus(HttpStatus.OK.getReasonPhrase());
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus("Failed");
            response.setMessage(e.getMessage());
            response.setResult("");
        }
        return response;
    }
   public Response deleteDateUsers(String id)
    {
        Response response = new Response();
        try{
            response = usersValidator.getUsersValidator(id);
            if(response.getResult() ==null) {
                Users accountStatementCredit = usersRepository.findBySocialSecurityNumberAndIsDeleted(String.format("%05d", Integer.valueOf(id)), false);
                accountStatementCredit.setIsDeleted(true);
                accountStatementCredit.setDateUpdated(new Date());
                usersRepository.save(accountStatementCredit);
                response.setResult(accountStatementCredit);
                response.setCode(200L);
                response.setMessage("Success Get data Users");
                response.setStatus(HttpStatus.OK.getReasonPhrase());
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus("Failed");
            response.setMessage(e.getMessage());
            response.setResult("");
        }
        return response;
    }

    public Response getDateUsersAll()
    {
        Response response = new Response();
        try{
            response = usersValidator.getUsersValidatorAll();
            if(response.getResult() ==null) {
                List<Users> usersList = usersRepository.findByIsDeletedFalse();
                response.setResult(usersList);
                response.setCode(200L);
                response.setMessage("data");
                response.setStatus(HttpStatus.OK.getReasonPhrase());
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus("Failed");
            response.setMessage(e.getMessage());
            response.setResult("");
        }
        return response;
    }

    public Response getDateUsersAllDel()
    {
        Response response = new Response();
        try{
            response = usersValidator.getUsersValidatorAllDel();
            if(response.getResult() ==null) {
                List<Users> usersList = usersRepository.findByIsDeletedTrue();
                response.setResult(usersList);
                response.setCode(200L);
                response.setMessage("data");
                response.setStatus(HttpStatus.OK.getReasonPhrase());
            }
        }catch (Exception e){
            e.printStackTrace();
            response.setStatus("Failed");
            response.setMessage(e.getMessage());
            response.setResult("");
        }
        return response;
    }
}
