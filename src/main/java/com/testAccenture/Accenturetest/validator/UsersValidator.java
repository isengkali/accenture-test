package com.testAccenture.Accenturetest.validator;

import com.testAccenture.Accenturetest.Entity.Dto.UsersInsertBodyDto;
import com.testAccenture.Accenturetest.Entity.Users;
import com.testAccenture.Accenturetest.Repository.UsersRepository;
import com.testAccenture.Accenturetest.Util.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class UsersValidator {
    private final UsersRepository usersRepository;

    public UsersValidator(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public Response addUsersValidator(UsersInsertBodyDto usersInsertBodyDto) {
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        Pattern namePattern = Pattern.compile("^[a-zA-Z0-9\\s]+$");
        Response response = new Response();
        if (StringUtils.isEmpty(usersInsertBodyDto.getSocialSecurityNumber())) {
            response.setMessage("Invalid value for field" + " Social Security Number," + " rejected value: Null");
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if(!pattern.matcher(usersInsertBodyDto.getSocialSecurityNumber()).matches()){
            response.setMessage("Invalid value for field" + " Social Security Number," + " rejected value: "+usersInsertBodyDto.getSocialSecurityNumber());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if (StringUtils.isEmpty(usersInsertBodyDto.getName())) {
            response.setMessage("Invalid value for field" + " Name," + " rejected value: Null");
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if(!namePattern.matcher(usersInsertBodyDto.getName()).matches()){
            response.setMessage("Invalid value for field" + " Name," + " rejected value: "+usersInsertBodyDto.getName());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if (StringUtils.isEmpty(usersInsertBodyDto.getDateOfBirth())){
            response.setMessage("Invalid value for field" + " Date Of Birth," + " rejected value: Null");
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        try{
            dateFormat.parse(usersInsertBodyDto.getDateOfBirth().trim());
        }catch (Exception e){
            response.setMessage("Invalid value for field" + " Date of Birth," + " rejected value: "+usersInsertBodyDto.getDateOfBirth());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        if (usersInsertBodyDto.getName().length()<2 && usersInsertBodyDto.getName().length()>50){
            response.setMessage("Invalid value for field"+" Name,"+" rejected value: "+usersInsertBodyDto.getName());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        Users usersList = usersRepository.findBySocialSecurityNumber(String.format("%05d", Integer.valueOf(usersInsertBodyDto.getSocialSecurityNumber())));
        if (usersList != null ) {
            response.setMessage("Record with SSN "+ usersInsertBodyDto.getSocialSecurityNumber() +" already exists in the system");
            response.setCode(30002l);
            response.setResult("");
            response.setStatus(HttpStatus.CONFLICT.getReasonPhrase());
            return response;
        }

        if(usersInsertBodyDto.getCreatedBy() != null &&!namePattern.matcher(usersInsertBodyDto.getCreatedBy()).matches()){
            response.setMessage("Invalid value for field" + " Created By," + " rejected value: "+usersInsertBodyDto.getCreatedBy());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if(usersInsertBodyDto.getUpdatedBy() != null &&!namePattern.matcher(usersInsertBodyDto.getUpdatedBy()).matches()){
            response.setMessage("Invalid value for field" + " Update By," + " rejected value: "+usersInsertBodyDto.getUpdatedBy());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        return response;
    }


    public Response updateUsersValidator(UsersInsertBodyDto usersInsertBodyDto,String id) {
        Response response = new Response();
        Pattern pattern = Pattern.compile("-?\\d+(\\.\\d+)?");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        Pattern namePattern = Pattern.compile("^[a-zA-Z0-9\\s]+$");

        if (StringUtils.isEmpty(id)) {
            response.setMessage("Invalid value for field" + " Social Security Number," + " rejected value: Null");
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        if(!pattern.matcher(id).matches()){
            response.setMessage("Invalid value for field" + " Social Security Number," + " rejected value: "+id);
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        Users usersList = usersRepository.findBySocialSecurityNumber(String.format("%05d", Integer.valueOf(id)));
        if (usersList == null) {
            response.setMessage("No such resource with id "+ id);
            response.setCode(30000l);
            response.setResult("");
            response.setStatus(HttpStatus.NOT_FOUND.getReasonPhrase());
            return response;
        }
        try{
            if(usersInsertBodyDto.getDateOfBirth() != null){
                dateFormat.parse(usersInsertBodyDto.getDateOfBirth().trim());
            }
        }catch (Exception e){
            response.setMessage("Invalid value for field" + " Date of Birth," + " rejected value: "+usersInsertBodyDto.getDateOfBirth());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        if(usersInsertBodyDto.getName()!= null && !namePattern.matcher(usersInsertBodyDto.getName()).matches()){
            response.setMessage("Invalid value for field" + " Name," + " rejected value: "+usersInsertBodyDto.getSocialSecurityNumber());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        if (usersInsertBodyDto.getName()!=null &&usersInsertBodyDto.getName().length()<2 && usersInsertBodyDto.getName().length()>50){
            response.setMessage("Invalid value for field"+" Name,"+" rejected value: "+usersInsertBodyDto.getName());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }

        if(usersInsertBodyDto.getCreatedBy() != null &&!namePattern.matcher(usersInsertBodyDto.getCreatedBy()).matches()){
            response.setMessage("Invalid value for field" + " Created By," + " rejected value: "+usersInsertBodyDto.getCreatedBy());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        if(usersInsertBodyDto.getUpdatedBy() != null &&!namePattern.matcher(usersInsertBodyDto.getUpdatedBy()).matches()){
            response.setMessage("Invalid value for field" + " Update By," + " rejected value: "+usersInsertBodyDto.getUpdatedBy());
            response.setCode(30001l);
            response.setResult("");
            response.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
            return response;
        }
        return response;
    }

    public Response getUsersValidator(String id){
        Response response = new Response();
        List<Users> usersList = usersRepository.findBySocialSecurityNumberAndIsDeletedFalse(String.format("%05d", Integer.valueOf(id)));
        if (usersList.isEmpty()||usersList.size()<1) {
            response.setMessage("No such resource with id "+ id);
            response.setCode(30000l);
            response.setResult("");
            response.setStatus(HttpStatus.NOT_FOUND.getReasonPhrase());
            return response;
        }
        return response;
    }

    public Response getUsersValidatorAll(){
        Response response = new Response();
        List<Users> usersList = usersRepository.findByIsDeletedFalse();
        if (usersList.isEmpty()||usersList.size()<1) {
            response.setMessage("No such resource");
            response.setCode(30000l);
            response.setResult("");
            response.setStatus(HttpStatus.NOT_FOUND.getReasonPhrase());
            return response;
        }
        return response;
    }

    public Response getUsersValidatorAllDel(){
        Response response = new Response();
        List<Users> usersList = usersRepository.findByIsDeletedTrue();
        if (usersList.isEmpty()||usersList.size()<1) {
            response.setMessage("No such resource");
            response.setCode(30000l);
            response.setResult("");
            response.setStatus(HttpStatus.NOT_FOUND.getReasonPhrase());
            return response;
        }
        return response;
    }
}
