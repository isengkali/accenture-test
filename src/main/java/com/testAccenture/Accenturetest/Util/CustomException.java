package com.testAccenture.Accenturetest.Util;

public class CustomException extends Exception{
    public CustomException(String str){
        super(str);
    }
}