package com.testAccenture.Accenturetest.Repository;

import com.testAccenture.Accenturetest.Entity.Users;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UsersRepository extends PagingAndSortingRepository<Users, String> {
    Users findBySocialSecurityNumber(String socialSecurityNumber);
    List<Users> findBySocialSecurityNumberAndIsDeletedFalse(String socialSecurityNumber);
    Users findBySocialSecurityNumberAndIsDeleted (String socialSecurityNumber, Boolean deleted);
    List<Users> findByIsDeletedFalse();
    List<Users> findByIsDeletedTrue();
}
